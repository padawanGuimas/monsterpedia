import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { NavbarTipoCreateComponent } from '../navbar-tipo-create/navbar-tipo-create.component';

@Component({
  selector: 'app-tipos-list',
  standalone: true,
  imports: [ CommonModule, NavbarTipoCreateComponent],
  templateUrl: './tipos-list.component.html',
  styleUrl: './tipos-list.component.css'
})
export class TiposListComponent {

  //TODO remover, isso vai vim do backend
  tipos = [
    { name: 'fogo' },
    { name: 'agua' },
    { name: 'planta'},
    { name: 'raio' },
    { name: 'normal' },
    { name: 'desconhecido' }


  ];

  edit(tipo: Tipo) {
    // código para editar o tipo
  }
  
  delete(tipo: Tipo) {
    // código para deletar o tipo
  }
  
}

export interface Tipo {
  name: string;
}

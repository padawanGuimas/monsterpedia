import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MonstrosListComponent } from './monstros-list/monstros-list.component';
import { TiposListComponent } from './tipos-list/tipos-list.component';
import { TiposCreateComponent } from './tipos-create/tipos-create.component';

export const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' }, //default route
    { path: 'home', component: HomeComponent },
    { path: 'tipos-list', component: TiposListComponent },
    { path: 'monstros-list', component: MonstrosListComponent },
    { path: 'tipos-create', component: TiposCreateComponent }
];


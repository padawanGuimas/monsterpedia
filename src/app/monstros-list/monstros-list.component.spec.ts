import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonstrosListComponent } from './monstros-list.component';

describe('MonstrosListComponent', () => {
  let component: MonstrosListComponent;
  let fixture: ComponentFixture<MonstrosListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MonstrosListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MonstrosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
